package fifteen

object Direction extends Enumeration {
  
  type Direction = Value
  val LEFT, RIGHT, DOWN, UP = Value
  
  def getDirectionByKeyCode(keyCode: Int): Direction = {
    keyCode match {
      case 2  => LEFT
      case 6  => RIGHT
      case 16 => UP
      case 14 => DOWN
      case _  => null
    }
  }
}
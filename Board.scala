package fifteen

import scala.util.Random
import fifteen.Direction._

object Board {
  
  val NUM = 4
  val SHAKE_NUMBER = NUM * 25
  var tmpShift = 1-NUM
  var boardArray = Array.fill(NUM)({
      // fill array(NUM, NUM) from 1 to NUM*NUM (actually 4*4)
      tmpShift += NUM
      Array.range(tmpShift, tmpShift + NUM)
  })
  boardArray(NUM-1)(NUM-1) = 0 // empty dice marked 0
  val winArray = boardArray.clone.flatten // array for check win combination
  var zeroPos = Map("x" -> (NUM-1), "y" -> (NUM-1))
  var statistic = 0       // count of movements
  var shakeStatistic = 0  // how much movement has been made when board mixed
  
  def updateShakeStatistic={
    shakeStatistic = statistic  // after mix take statistic
    statistic = 0               // and begin count again gamer movement
  }
  
  def mix(){
    val rand = new Random();
    // from correct position randomly move dice to mix board
    for(_ <- 1 to SHAKE_NUMBER){
      move(Direction.apply(rand.nextInt(4))) // max value of direction = 4
    }
    updateShakeStatistic
  }

  // move dice to empty position
  def move(dir: Direction){
    val x = zeroPos.apply("x")
    val y = zeroPos.apply("y")
    dir match{
      case RIGHT => {
        if (y != 0){
          swap(x, y, x, y-1)
        }
      }
      case LEFT => {
        if (y != NUM-1){
          swap(x, y, x, y+1)
        }
      }
      case UP => {
        if (x != NUM-1){
          swap(x, y, x+1, y)
        }
      }
      case DOWN => {
        if (x != 0){
          swap(x, y, x-1, y)
        }
      }
    }
  }
  
  def swap(firstX: Int, firstY: Int, secondX: Int, secondY: Int){
    val tmp = boardArray(firstX)(firstY)
    boardArray(firstX)(firstY) = boardArray(secondX)(secondY)
    boardArray(secondX)(secondY) = tmp
    zeroPos = 
      if (boardArray(firstX)(firstY) == 0)
        Map("x" -> firstX, "y" -> firstY)
      else
        Map("x" -> secondX, "y" -> secondY)
    statistic += 1
  }
  
  def isWin: Boolean = {
    if (winArray.sameElements(boardArray.flatten[Int]))
      true
    else false
  }
}

package fifteen

object BoardPrint {

  def printBoard(boardArray: Array[Array[Int]], shakeStatistic: Int, statistic: Int){
    print("\033[2J")
    println("Enter 'Escape' to exit")
    println("Use up, left, right, down keys to move dice to empty position")
    println("Movement count to mix: " + shakeStatistic)
    println("Movement count: " + statistic)
    println("---------------------")
    boardArray.foreach{lineArray =>
      print("| ")
      lineArray.foreach{dice=>
        if (dice == 0) print(" ")
        else print(dice)
        if (dice < 10) print(" ")
        print(" | ")
      }
      println
    }
    println("---------------------")
  }
  
  def printWin{
    println("Congratulations! You won!")
  }  
  
}

package fifteen

import tools.jline.console.ConsoleReader

object GameControl {
  
  val EXIT_KEY = 27 // Escape
  val console = new tools.jline.console.ConsoleReader()
 
  def run{
    Board.mix
    BoardPrint.printBoard(Board.boardArray, Board.shakeStatistic, Board.statistic)
    var key = 0
    while(key != EXIT_KEY){
      key = console.readVirtualKey()
      val dir = Direction.getDirectionByKeyCode(key)
      if (dir != null){
        Board.move(dir)
        BoardPrint.printBoard(Board.boardArray, Board.shakeStatistic, Board.statistic)
        if (Board.isWin){
          BoardPrint.printWin
          key = EXIT_KEY
        }
      }
    }
  }
}
